CREATE DATABASE  IF NOT EXISTS `prekyboscentras` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `prekyboscentras`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: prekyboscentras
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `kasininkai`
--

DROP TABLE IF EXISTS `kasininkai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `kasininkai` (
  `KasininkoId` int(11) NOT NULL,
  `KasininkoNumeris` int(11) DEFAULT NULL,
  PRIMARY KEY (`KasininkoId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kasininkai`
--

LOCK TABLES `kasininkai` WRITE;
/*!40000 ALTER TABLE `kasininkai` DISABLE KEYS */;
/*!40000 ALTER TABLE `kasininkai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kasos`
--

DROP TABLE IF EXISTS `kasos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `kasos` (
  `KasosId` int(11) NOT NULL,
  `ParduotuvesId` int(11) DEFAULT NULL,
  `KasosNumeris` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`KasosId`),
  KEY `ParduotuvesId` (`ParduotuvesId`),
  CONSTRAINT `kasos_ibfk_1` FOREIGN KEY (`ParduotuvesId`) REFERENCES `parduotuves` (`ParduotuvesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kasos`
--

LOCK TABLES `kasos` WRITE;
/*!40000 ALTER TABLE `kasos` DISABLE KEYS */;
/*!40000 ALTER TABLE `kasos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kasoskasininkai`
--

DROP TABLE IF EXISTS `kasoskasininkai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `kasoskasininkai` (
  `KasosKasininkoId` int(11) NOT NULL,
  `KasininkoId` int(11) DEFAULT NULL,
  `KasosId` int(11) DEFAULT NULL,
  PRIMARY KEY (`KasosKasininkoId`),
  KEY `FK_Kasos_KasosId` (`KasosId`),
  KEY `FK_Kasininkai_KasininkoId` (`KasininkoId`),
  CONSTRAINT `FK_Kasininkai_KasininkoId` FOREIGN KEY (`KasininkoId`) REFERENCES `kasininkai` (`KasininkoId`),
  CONSTRAINT `FK_Kasos_KasosId` FOREIGN KEY (`KasosId`) REFERENCES `kasos` (`KasosId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kasoskasininkai`
--

LOCK TABLES `kasoskasininkai` WRITE;
/*!40000 ALTER TABLE `kasoskasininkai` DISABLE KEYS */;
/*!40000 ALTER TABLE `kasoskasininkai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parduotuves`
--

DROP TABLE IF EXISTS `parduotuves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `parduotuves` (
  `ParduotuvesId` int(11) NOT NULL,
  `Miestas` varchar(200) DEFAULT NULL,
  `Adresas` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ParduotuvesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parduotuves`
--

LOCK TABLES `parduotuves` WRITE;
/*!40000 ALTER TABLE `parduotuves` DISABLE KEYS */;
/*!40000 ALTER TABLE `parduotuves` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-02 17:41:25
